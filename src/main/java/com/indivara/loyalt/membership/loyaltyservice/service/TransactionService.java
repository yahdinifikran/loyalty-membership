package com.indivara.loyalt.membership.loyaltyservice.service;

import com.indivara.loyalt.membership.loyaltyservice.entity.Product;
import com.indivara.loyalt.membership.loyaltyservice.entity.Transaction;
import com.indivara.loyalt.membership.loyaltyservice.entity.Users;
import com.indivara.loyalt.membership.loyaltyservice.model.TransactionRequest;
import com.indivara.loyalt.membership.loyaltyservice.model.TransactionResponse;
import com.indivara.loyalt.membership.loyaltyservice.repository.ProductRepository;
import com.indivara.loyalt.membership.loyaltyservice.repository.TransactionRepository;
import com.indivara.loyalt.membership.loyaltyservice.repository.UsersRepository;
import com.indivara.loyalt.membership.loyaltyservice.util.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class TransactionService {
    @Autowired
    TransactionRepository transactionRepository;
    @Autowired
    UsersRepository usersRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    UsersService usersService;

    public TransactionResponse saveTransaction(String token, TransactionRequest request){

        Optional<Users> usersToken = usersService.getToken(token);
        if (!usersToken.isPresent()){
            throw new BusinessException("User token not found");
        }

        Transaction transaction = new Transaction();
        transaction.setUsers(getUserId(request.getIdUser()));
        transaction.setProduct(getProductId(request.getIdProduct()));
        transaction.setPaid(request.getPaid());
        transaction.setIdVoucher(request.getIdVoucher());
        transaction.setCreateDate(new Date());
        Transaction t = transactionRepository.save(transaction);

        TransactionResponse response = new TransactionResponse();
        response.setIdUser(t.getUsers().getIdUsers());
        response.setIdProduct(t.getProduct().getIdProduct());
        response.setPaid(t.getPaid());
        response.setIdVoucher(t.getIdVoucher());
        response.setCreateDate(t.getCreateDate());
        return response;


    }
    private Users getUserId(Integer idUsers){
        Users users = usersRepository.findById(idUsers)
                .orElseThrow(()-> new BusinessException("Not found user id"));
        return users;
    }
    private Product getProductId(Integer idProduct){
        Product product = productRepository.findById(idProduct)
                .orElseThrow(() -> new BusinessException("Not found product id"));
        return product;
    }
}
