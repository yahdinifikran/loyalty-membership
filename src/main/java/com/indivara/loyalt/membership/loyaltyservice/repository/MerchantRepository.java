package com.indivara.loyalt.membership.loyaltyservice.repository;

import com.indivara.loyalt.membership.loyaltyservice.entity.Merchant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MerchantRepository extends JpaRepository<Merchant, Integer>{
}
