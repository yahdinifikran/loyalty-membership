package com.indivara.loyalt.membership.loyaltyservice.service;

import com.indivara.loyalt.membership.loyaltyservice.entity.Voucher;
import com.indivara.loyalt.membership.loyaltyservice.model.VoucherRequest;
import com.indivara.loyalt.membership.loyaltyservice.model.VoucherResponse;
import com.indivara.loyalt.membership.loyaltyservice.repository.VoucherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VoucherService {
    @Autowired
    VoucherRepository voucherRepository;

    public VoucherResponse saveVoucher(VoucherRequest request){
        Voucher v = new Voucher();
        v.setNameVoucher(request.getNameVoucher());
        v.setDescription(request.getDescription());
        v.setPrice(request.getPrice());
        v.setExpiredDate(request.getExpiredDate());

        Voucher voucher = voucherRepository.save(v);
        VoucherResponse response = new VoucherResponse();
        response.setNameVoucher(voucher.getNameVoucher());
        response.setDescription(voucher.getDescription());
        response.setPrice(voucher.getPrice());
        response.setExpiredDate(voucher.getExpiredDate());
        return response;
    }
}
