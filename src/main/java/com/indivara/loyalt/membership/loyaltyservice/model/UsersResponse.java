package com.indivara.loyalt.membership.loyaltyservice.model;

import lombok.Data;

@Data
public class UsersResponse {
    private String token;
}
