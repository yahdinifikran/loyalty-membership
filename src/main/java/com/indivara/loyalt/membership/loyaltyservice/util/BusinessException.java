package com.indivara.loyalt.membership.loyaltyservice.util;

public class BusinessException extends RuntimeException {
    public BusinessException(String message){
        super(message);
    }

}
