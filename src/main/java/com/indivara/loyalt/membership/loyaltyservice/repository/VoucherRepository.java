package com.indivara.loyalt.membership.loyaltyservice.repository;

import com.indivara.loyalt.membership.loyaltyservice.entity.Voucher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VoucherRepository extends JpaRepository<Voucher, Integer> {

}
