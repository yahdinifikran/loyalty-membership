package com.indivara.loyalt.membership.loyaltyservice.model;

import lombok.Data;

@Data
public class RegisterRequest {
    private String fullName;
    private String phone;
    private String email;
    private String address;
}
