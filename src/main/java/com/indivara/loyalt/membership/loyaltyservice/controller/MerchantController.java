package com.indivara.loyalt.membership.loyaltyservice.controller;

import com.indivara.loyalt.membership.loyaltyservice.model.MerchantRequest;
import com.indivara.loyalt.membership.loyaltyservice.model.MerchantResponse;
import com.indivara.loyalt.membership.loyaltyservice.service.MerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/loyalty-merchant")
public class MerchantController {
    @Autowired
    MerchantService merchantService;

    @PostMapping("/save")
    public MerchantResponse saveMerchant(@RequestBody MerchantRequest request){
        return merchantService.saveMerchant(request);
    }
}
