package com.indivara.loyalt.membership.loyaltyservice.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class Voucher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idVoucher;
    private String nameVoucher;
    private String description;
    private String price;
    private String expiredDate;
}
