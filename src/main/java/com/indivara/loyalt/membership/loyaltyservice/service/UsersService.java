package com.indivara.loyalt.membership.loyaltyservice.service;

import com.indivara.loyalt.membership.loyaltyservice.entity.Users;
import com.indivara.loyalt.membership.loyaltyservice.model.RegisterRequest;
import com.indivara.loyalt.membership.loyaltyservice.model.RegisterResponse;
import com.indivara.loyalt.membership.loyaltyservice.model.UsersRequest;
import com.indivara.loyalt.membership.loyaltyservice.model.UsersResponse;
import com.indivara.loyalt.membership.loyaltyservice.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class UsersService {
    @Autowired
    UsersRepository usersRepository;

    public RegisterResponse registration(RegisterRequest request){
        Users u = new Users();
        u.setFullName(request.getFullName());
        u.setAddress(request.getAddress());
        u.setPhone(request.getPhone());
        u.setEmail(request.getEmail());

        Users users = usersRepository.save(u);
        RegisterResponse response = new RegisterResponse();
        response.setFullName(users.getFullName());
        response.setAddress(users.getAddress());
        response.setPhone(users.getPhone());
        response.setEmail(users.getEmail());
        return response;
    }

    public UsersResponse login(UsersRequest request){
        Optional<Users> users = usersRepository.login(request.getEmail());
        String token = UUID.randomUUID().toString();
        Users user = users.get();
        user.setEmail(request.getEmail());
        user.setToken(token);
        Users uResponse = usersRepository.save(user);

        UsersResponse response = new UsersResponse();
        response.setToken(uResponse.getToken());
        return response;
    }
    public Optional<Users> getToken(String token){
        return usersRepository.findByToken(token);
    }
}
