package com.indivara.loyalt.membership.loyaltyservice.repository;

import com.indivara.loyalt.membership.loyaltyservice.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsersRepository extends JpaRepository<Users, Integer> {
    @Query(value = "SELECT u FROM Users u where u.email = ?1")
    Optional<Users> login(String email);

    Optional<Users> findByToken(String token);
}
