package com.indivara.loyalt.membership.loyaltyservice.controller;

import com.indivara.loyalt.membership.loyaltyservice.model.TransactionRequest;
import com.indivara.loyalt.membership.loyaltyservice.model.TransactionResponse;
import com.indivara.loyalt.membership.loyaltyservice.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/loyalty-transaction")
public class TransactionController {

    @Autowired
    TransactionService transactionService;

    @PostMapping("/save")
    public TransactionResponse saveTransaction(@RequestHeader("X-HEAXDER-TOKEN") String token, @RequestBody TransactionRequest request){
        return transactionService.saveTransaction(token, request);
    }
}
