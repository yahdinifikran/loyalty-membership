package com.indivara.loyalt.membership.loyaltyservice.controller;

import com.indivara.loyalt.membership.loyaltyservice.model.VoucherRequest;
import com.indivara.loyalt.membership.loyaltyservice.model.VoucherResponse;
import com.indivara.loyalt.membership.loyaltyservice.service.VoucherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/loyalty-voucher")
public class VoucherController {

    @Autowired
    VoucherService voucherService;

    @PostMapping("/save")
    public VoucherResponse saveVoucher(@RequestBody VoucherRequest request){
        return voucherService.saveVoucher(request);
    }
}
