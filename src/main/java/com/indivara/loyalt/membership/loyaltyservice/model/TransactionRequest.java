package com.indivara.loyalt.membership.loyaltyservice.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TransactionRequest {

    private Integer idUser;
    private Integer idProduct;
    private BigDecimal paid;
    private Integer idVoucher;

}
