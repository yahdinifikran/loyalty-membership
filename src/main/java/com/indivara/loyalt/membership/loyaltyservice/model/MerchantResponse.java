package com.indivara.loyalt.membership.loyaltyservice.model;

import lombok.Data;

@Data
public class MerchantResponse {
    private String name;
    private String email;
    private String phone;
}
