package com.indivara.loyalt.membership.loyaltyservice.service;

import com.indivara.loyalt.membership.loyaltyservice.entity.Merchant;
import com.indivara.loyalt.membership.loyaltyservice.model.MerchantRequest;
import com.indivara.loyalt.membership.loyaltyservice.model.MerchantResponse;
import com.indivara.loyalt.membership.loyaltyservice.repository.MerchantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MerchantService {

    @Autowired
    MerchantRepository merchantRepository;

    public MerchantResponse saveMerchant(MerchantRequest request){
        Merchant m = new Merchant();
        m.setName(request.getName());
        m.setEmail(request.getEmail());
        m.setPhone(request.getPhone());
        Merchant mResponse = merchantRepository.save(m);

        MerchantResponse response = new MerchantResponse();
        response.setName(mResponse.getName());
        response.setEmail(mResponse.getEmail());
        response.setPhone(mResponse.getPhone());
        return response;


    }
}
