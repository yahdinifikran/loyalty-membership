package com.indivara.loyalt.membership.loyaltyservice.model;

import lombok.Data;

@Data
public class VoucherRequest {
        private String nameVoucher;
        private String description;
        private String price;
        private String expiredDate;
    }
