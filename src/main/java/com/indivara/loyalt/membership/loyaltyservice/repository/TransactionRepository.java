package com.indivara.loyalt.membership.loyaltyservice.repository;

import com.indivara.loyalt.membership.loyaltyservice.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

}
