package com.indivara.loyalt.membership.loyaltyservice.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductResponse {
    private String nameProduct;
    private BigDecimal price;
    private Integer idMerchant;

}
