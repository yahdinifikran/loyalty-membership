package com.indivara.loyalt.membership.loyaltyservice.controller;

import com.indivara.loyalt.membership.loyaltyservice.model.ProductRequest;
import com.indivara.loyalt.membership.loyaltyservice.model.ProductResponse;
import com.indivara.loyalt.membership.loyaltyservice.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("loyalty-product")
public class ProductController {

    @Autowired
    ProductService productService;
    @PostMapping("/save")
    public ProductResponse saveProduct(@RequestBody ProductRequest request){
        return productService.saveProduct(request);
    }
}
