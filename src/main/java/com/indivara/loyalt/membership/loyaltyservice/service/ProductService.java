package com.indivara.loyalt.membership.loyaltyservice.service;

import com.indivara.loyalt.membership.loyaltyservice.entity.Merchant;
import com.indivara.loyalt.membership.loyaltyservice.entity.Product;
import com.indivara.loyalt.membership.loyaltyservice.model.ProductRequest;
import com.indivara.loyalt.membership.loyaltyservice.model.ProductResponse;
import com.indivara.loyalt.membership.loyaltyservice.repository.MerchantRepository;
import com.indivara.loyalt.membership.loyaltyservice.repository.ProductRepository;
import com.indivara.loyalt.membership.loyaltyservice.util.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;

    @Autowired
    MerchantRepository merchantRepository;

    public ProductResponse saveProduct(ProductRequest request){
        Merchant mId = merchantRepository.findById(request.getIdMerchant())
                .orElseThrow(() -> new BusinessException("Not found id"));

        Product product = new Product();
        product.setNameProduct(request.getNameProduct());
        product.setPrice(request.getPrice());
        product.setMerchant(mId);
        productRepository.save(product);

        ProductResponse response = new ProductResponse();
        response.setNameProduct(request.getNameProduct());
        response.setPrice(request.getPrice());
        response.setIdMerchant(request.getIdMerchant());
        return response;
    }
}
