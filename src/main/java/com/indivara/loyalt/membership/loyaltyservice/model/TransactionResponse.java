package com.indivara.loyalt.membership.loyaltyservice.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class TransactionResponse {

    private Integer idUser;
    private Integer idProduct;
    private BigDecimal paid;
    private Integer idVoucher;
    private Date createDate;

}
