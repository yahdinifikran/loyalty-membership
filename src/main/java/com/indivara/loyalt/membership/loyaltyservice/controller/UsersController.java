package com.indivara.loyalt.membership.loyaltyservice.controller;

import com.indivara.loyalt.membership.loyaltyservice.model.RegisterRequest;
import com.indivara.loyalt.membership.loyaltyservice.model.RegisterResponse;
import com.indivara.loyalt.membership.loyaltyservice.model.UsersRequest;
import com.indivara.loyalt.membership.loyaltyservice.model.UsersResponse;
import com.indivara.loyalt.membership.loyaltyservice.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/loyalty-users")
public class UsersController {

    @Autowired
    private UsersService usersService;

    @PostMapping("/registration")
    public RegisterResponse registration(@RequestBody RegisterRequest request){
        return usersService.registration(request);
    }

    @PostMapping("/login")
    public UsersResponse login(@RequestBody UsersRequest request){
        return usersService.login(request);
    }
}
